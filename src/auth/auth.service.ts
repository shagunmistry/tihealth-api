import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';

import { RegistrationService } from '../registration/registration.service';
import {
  BaseRegistrationSchema,
  RegistrationDocument,
} from '../registration/models/registration.schema';
import { Role } from '../shared/enums_decorators/role';
import { IBasicForm } from '../shared/interfaces.shared';

type ValidateUserReturnType = {
  userBirthday: Date;
  userEmail: string;
  roles: Role[];
  firstName: string;
  lastName: string;
  _id: string;
  __v: string;
} | null;

@Injectable()
export class AuthService {
  constructor(
    private registrationService: RegistrationService,
    private jwtService: JwtService,
  ) {}

  getLoginForm(): IBasicForm {
    return {
      formId: 'loginForm',
      title: 'Login',
      fields: [
        {
          fieldID: 'userEmail',
          fieldTitle: 'Email',
          fieldType: 'input',
        },
        {
          fieldID: 'password',
          fieldTitle: 'Password',
          fieldType: 'password',
        },
      ],
    };
  }

  async registerUser(body: BaseRegistrationSchema): Promise<any> {
    return await this.registrationService.createUserRegistration(body);
  }

  /**
   *  Validate to see if a user with the given email address exists.
   * @param email string
   * @param pass string
   * @returns ValidateUserReturnType
   */
  async validateUser(
    email: string,
    pass: string,
  ): Promise<ValidateUserReturnType> {
    const user = await this.registrationService.checkUserRegistration(email);
    const isMatch = user ? await bcrypt.compare(pass, user.password) : false;

    if (user && user._id && isMatch) {
      return {
        __v: user.__v,
        _id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        roles: [...user.roles],
        userBirthday: user.userBirthday,
        userEmail: user.userEmail,
      };
    }

    return null;
  }

  /**
   * Login a user and return a jwt token.
   * @param user
   * @returns
   */
  async login(user: RegistrationDocument) {
    const payload = { ...user };
    return {
      access_token: this.jwtService.sign(payload, { expiresIn: 60 * 60 }),
    };
  }
}
