import {
  Body,
  Controller,
  Get,
  Logger,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';

import { AuthService } from './auth.service';
import { BaseRegistrationSchema } from '../registration/models/registration.schema';
import { JwtAuthGuard, LocalAuthGuard } from './guards-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Get()
  healthCheck() {
    return 'Hello from Auth Controller';
  }

  @ApiOperation({ summary: 'Get the login form.' })
  @Get('login/form')
  getLoginForm() {
    return this.authService.getLoginForm();
  }

  @ApiOperation({ summary: 'Allow user to register for the application' })
  @Post('register')
  async register(@Body() body: BaseRegistrationSchema) {
    return await this.authService.registerUser(body);
  }

  @ApiOperation({ summary: 'Allow user to log in to the application' })
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile/:userEmail')
  getProfile(@Request() req) {
    const userEmail = req.params.userEmail;
    Logger.debug(`Returning GetProfile for ${userEmail}`, 'getProfile');
    return req.user;
  }
}
