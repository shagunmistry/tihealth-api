import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';

import {
  BaseRegistrationSchema,
  PatientRegistrationSchema,
  PatientRegistrationSchemaForClass,
  RegistrationSchemaForClass,
} from '../registration/models/registration.schema';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { RegistrationService } from '../registration/registration.service';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '60s' },
    }),
    MongooseModule.forFeature([
      { name: BaseRegistrationSchema.name, schema: RegistrationSchemaForClass },
      {
        name: PatientRegistrationSchema.name,
        schema: PatientRegistrationSchemaForClass,
      },
    ]),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, RegistrationService, JwtStrategy],
})
export class AuthModule {}
