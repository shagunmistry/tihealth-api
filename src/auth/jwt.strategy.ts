import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    const data = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    };
    super(data);
  }

  async validate(payload: any) {
    Logger.debug(
      `User payload: ${JSON.stringify(payload)}`,
      'Jwt Strategy - Validate',
    );
    return { ...payload };
  }
}
