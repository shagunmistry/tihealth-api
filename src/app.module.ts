import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { DoctorModule } from './doctor/doctor.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PatientModule } from './patient/patient.module';
import { ThrottlerModule } from '@nestjs/throttler';
import { RegistrationModule } from './registration/registration.module';
import { AuthModule } from './auth/auth.module';
import { FileuploadModule } from './fileupload/fileupload.module';

const dbConnectionString = `mongodb+srv://${
  process.env.MONGOOSE_CLUSTER0_USER
}:${
  process.env.MONGOOSE_CLUSTER0_PASS
}@tihealth-cluster0.awgig.mongodb.net/tiHealth-${
  process.env.NODE_ENV || 'test'
}?retryWrites=true&w=majority`;

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    MongooseModule.forRoot(dbConnectionString),
    AuthModule,
    DoctorModule,
    PatientModule,
    RegistrationModule,
    FileuploadModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
