require('dotenv').config({ path: '.env' });
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('TiHealth')
    .setDescription('API for TiHealth Application')
    .setVersion('1.0')
    .addTag('TiHealth')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);

  // const cors = {
  //   origin: ['*'],
  //   methods: 'GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS',
  //   allowedHeaders: ['Accept', 'Content-Type', 'Authorization'],
  // };

  app.enableCors();
  app.use(helmet());
  await app.listen(process.env.PORT || 5000);
}
bootstrap();
