require('dotenv').config({ path: '.env' });
import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
// import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as helmet from 'helmet';

export async function createApp(): Promise<INestApplication> {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');

  // const config = new DocumentBuilder()
  //   .setTitle('TiHealth')
  //   .setDescription('API for TiHealth Application')
  //   .setVersion('1.0')
  //   .addTag('TiHealth')
  //   .build();
  // const document = SwaggerModule.createDocument(app, config);
  // SwaggerModule.setup('swagger', app, document);

  app.enableCors();
  app.use(helmet());

  await app.init();
  return app;
}
