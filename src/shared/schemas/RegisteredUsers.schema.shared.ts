import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Schema } from '@nestjs/mongoose';

@Schema()
export class LoginSchema {
  @ApiProperty({ required: true })
  @Prop({ required: true })
  userEmail: string;

  @ApiProperty({ required: true })
  @Prop({ required: true })
  password: string;
}
