export interface PhoneNumbers {
  cell?: number;
  home?: number;
  office?: number;
}

export interface Name {
  firstName: string;
  middleName: string;
  lastName: string;
}

export interface Address {
  city: string;
  country: string;
  postalCode: string;
  state: string;
  streetName: string;
  streetNameTwo: string;
  addressType: string;
}

export interface Contact {
  relationship: string;
  name: Name;
  address: Address;
  gender: string;
}

export interface IBasicForm {
  title: string;
  formId: string;
  fields: IField[];
}

export interface IField {
  fieldType:
    | 'checkmark'
    | 'date'
    | 'dropdown'
    | 'file'
    | 'form'
    | 'group'
    | 'input'
    | 'label'
    | 'number'
    | 'password'
    | 'radioButton';
  fieldTitle: string;
  fieldID: string;
  fields?: IField[];
  required?: boolean;
}

export interface IGlobalResponse {
  error: boolean;
  message: string;
  id: string;
  data?: any;
}
