import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FileuploadController } from './fileupload.controller';
import { FileUploadSchemaForClass, SFileUpload } from './fileupload.schema';
import { FileuploadService } from './fileupload.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SFileUpload.name, schema: FileUploadSchemaForClass },
    ]),
  ],
  controllers: [FileuploadController],
  providers: [FileuploadService],
})
export class FileuploadModule {}
