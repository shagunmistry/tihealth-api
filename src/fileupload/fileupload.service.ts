import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  FileUploadDocument,
  FileUploadInterface,
  SFileUpload,
} from './fileupload.schema';
import { IGlobalResponse } from '../shared/interfaces.shared';

@Injectable()
export class FileuploadService {
  context = 'FileUpload Service';

  constructor(
    @InjectModel(SFileUpload.name)
    private readonly fileUploadModel: Model<FileUploadDocument>,
  ) {}

  /**
   * Get the uploaded files for a particular user.
   * @param userId
   * @returns
   */
  async getFilesForUser(userId: string): Promise<any> {
    try {
      const query = this.fileUploadModel.find({ for: userId });
      const res = await query.exec();
      if (res && res.length > 0) {
        return {
          length: res.length,
          files: res,
        };
      }
      return {
        length: 0,
        files: null,
        error: false,
        id: null,
        message: `There were no files for ${userId}`,
      };
    } catch (e: any) {
      Logger.error(
        `There was an error getting files for ${userId}: ${e.message}`,
        this.context,
      );
      return {
        error: true,
        message: `There was an error getting files for ${userId}: ${e.message}`,
        id: null,
      };
    }
  }

  /**
   * Get the uploaded files that were uploaded by a particular user.
   * @param userId
   * @returns
   */
  async getFileUploadedByUser(userId: string): Promise<any> {
    try {
      const query = this.fileUploadModel.find({ uploadBy: userId });
      const res = await query.exec();
      if (res && res.length > 0) {
        return {
          length: res.length,
          resWithFiles: res,
        };
      }
      return {
        length: 0,
        resWithFiles: null,
        error: false,
        id: null,
        message: `There were no files uploaded by ${userId}`,
      };
    } catch (e: any) {
      Logger.error(
        `There was an error getting files uploaded by ${userId}: ${e.message}`,
        this.context,
      );
      return {
        error: true,
        message: `There was an error getting files uploaded by ${userId}: ${e.message}`,
        id: null,
      };
    }
  }

  /**
   * Upload files for a particular user
   * @param body
   * @param files
   * @returns
   */
  async uploadFiles(
    body: FileUploadInterface,
    file: Express.Multer.File,
  ): Promise<IGlobalResponse | Promise<IGlobalResponse>[]> {
    Logger.debug(`Upload() --> Processing ${file} files`, this.context);

    try {
      const newFileModel = new this.fileUploadModel({
        ...body,
        file: file,
      });
      const res = await newFileModel.save().catch((e: any) => {
        throw new BadRequestException(e.message);
      });

      Logger.log(
        `File ${file.filename} uploaded for ${body.for} by ${body.uploadBy}.`,
        this.context,
      );
      return {
        error: false,
        message: 'Successfully uploaded file(s)!',
        id: res._id,
      };
    } catch (e: any) {
      Logger.error(
        `There was an error uploading files: ${e.message}`,
        this.context,
      );
      return {
        error: true,
        message: e.message,
        id: null,
      };
    }
  }
}
