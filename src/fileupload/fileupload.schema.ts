import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export interface FileUploadInterface {
  description: string;
  for: string;
  name: string;
  uploadBy: string;
  uploadTime: Date;
}

@Schema()
export class SFileUpload {
  @ApiProperty({
    description: 'Description of the File(s) being uploaded.',
  })
  @Prop({ required: true })
  description: String;

  @ApiProperty({ description: 'File or files being uploaded', required: true })
  @Prop({ type: 'Mixed', required: true })
  file: mongoose.Mixed;

  @ApiProperty({
    description: 'Who the file being uploaded for',
    required: true,
  })
  @Prop({ required: true })
  for: String;

  @ApiProperty({
    description: 'Name of the file',
    required: true,
  })
  @Prop({ required: true })
  name: String;

  @ApiProperty({ description: 'Who it is being uploaded by', required: true })
  @Prop({ required: true })
  uploadBy: String;

  @ApiProperty({ description: 'When the file was uploaded.', required: true })
  @Prop({ required: true })
  uploadTime: Date;
}

export type FileUploadDocument = SFileUpload & mongoose.Document;
export const FileUploadSchemaForClass = SchemaFactory.createForClass(
  SFileUpload,
);
