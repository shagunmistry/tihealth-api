import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { ApiResponse } from '@nestjs/swagger';

import { FileUploadInterface } from './fileupload.schema';
import { FileuploadService } from './fileupload.service';
import { JwtAuthGuard } from '../auth/guards-auth.guard';

@Controller('fileupload')
export class FileuploadController {
  constructor(private fileUploadService: FileuploadService) {}

  context = 'FileUpload Controller';

  @UseGuards(JwtAuthGuard)
  @Get()
  healthCheck() {
    return `Hello from ${this.context}!`;
  }

  // TODO: Do query parameters to filter by date.
  @UseGuards(JwtAuthGuard)
  @ApiResponse({
    status: 201,
    description: 'Files received successfully!',
  })
  @ApiResponse({
    status: 400,
    description: 'There was an error getting files.',
  })
  @ApiResponse({
    status: 401,
    description: 'There was an error authorizing to get files.',
  })
  @Get('files/:userId')
  async getFiles(
    @Param('userId') userId: string,
    @Query('method') method: string,
  ) {
    Logger.debug(`Getting documents for ${userId}`, this.context);
    try {
      if (method === 'uploadBy') {
        return await this.fileUploadService.getFileUploadedByUser(userId);
      }
      return await this.fileUploadService.getFilesForUser(userId);
    } catch (e: any) {
      Logger.error(
        `Getting files for ${userId} error: ${e.message}`,
        this.context,
      );
      throw new BadRequestException(`Files Get Error: ${e.message}`);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('upload')
  @UseInterceptors(AnyFilesInterceptor())
  @ApiResponse({
    status: 201,
    description: 'Files uploaded successfully!',
  })
  @ApiResponse({
    status: 400,
    description: 'There was an error uploading files.',
  })
  @ApiResponse({
    status: 401,
    description: 'There was an error authorizing to upload files.',
  })
  async uploadFile(
    @UploadedFiles() file: Express.Multer.File,
    @Body() uploadBody: FileUploadInterface,
  ): Promise<any> {
    try {
      if (file) {
        const res = await this.fileUploadService.uploadFiles(uploadBody, file);
        return res;
      } else {
        throw new BadRequestException('No valid files attached.');
      }
    } catch (e: any) {
      Logger.error(`File Upload error: ${e.message}`, this.context);
      throw new BadRequestException(
        `File Upload Error: ${e.message}. Please upload valid files only.`,
      );
    }
  }
}
