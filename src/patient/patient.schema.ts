import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  Address as IAddress,
  Contact,
  Name,
  PhoneNumbers,
} from '../shared/interfaces.shared';
import { Document } from 'mongoose';

export type PatientDocument = Patient & Document;

@Schema({ strict: false })
export class Patient {
  @Prop(
    raw({
      city: { type: String },
      country: { type: String },
      postalCode: { type: String },
      state: { type: String },
      streetName: { type: String },
      streetNameTwo: { type: String },
      addressType: { type: String },
    }),
  )
  address: IAddress;

  @Prop()
  birthday: Date;

  @Prop(
    raw({
      relationship: { type: String },
      name: { type: String },
      address: { type: String },
      gender: { type: String },
    }),
  )
  contact: Contact;

  @Prop()
  description: string;

  @Prop()
  gender: 'Male' | 'Female';

  @Prop()
  height: number;

  @Prop()
  heightType: string;

  @Prop()
  id: string;

  @Prop(
    raw({
      firstName: { type: String },
      middleName: { type: String },
      lastName: { type: String },
    }),
  )
  name: { type: Name };

  @Prop(
    raw({
      cell: { type: Number },
      home: { type: Number },
      office: { type: Number },
    }),
  )
  phoneNumbers: { type: PhoneNumbers };

  @Prop()
  sexuality: string;

  @Prop()
  emailAddress: string;

  @Prop()
  weight: number;

  @Prop()
  weightType: string;

  @Prop()
  forDoctorId: string;
}

export const PatientSchema = SchemaFactory.createForClass(Patient);
