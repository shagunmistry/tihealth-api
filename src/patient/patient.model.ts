import {
  Address as IAddress,
  Contact,
  Name,
  PhoneNumbers,
} from '../shared/interfaces.shared';

export class CreateOrUpdatePatientDto {
  address: IAddress;
  birthday: Date;
  contact: Contact;
  description: string;
  emailAddress: string;
  forDoctorId: string;
  gender: 'Male' | 'Female'; // TODO: Add more genders later on.
  height: number;
  heightType: string;
  id: string;
  name: Name;
  phoneNumbers: PhoneNumbers;
  sexuality: string;
  weight: number;
  weightType: string;
}

export class GetPatientDto {
  readonly emailAddress?: string;
  readonly id?: string;
}
