import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrUpdatePatientDto } from './patient.model';
import { PatientDocument, Patient } from './patient.schema';

@Injectable()
export class PatientService {
  constructor(
    @InjectModel(Patient.name)
    private patientModel: Model<PatientDocument>,
  ) {}

  async createPatient(
    createPatient: CreateOrUpdatePatientDto,
  ): Promise<Patient> {
    const createPatientModel = new this.patientModel(createPatient);
    return await createPatientModel.save();
  }

  async getPatient(id: string) {
    const res = await this.patientModel.findById(id).exec();
    return res;
  }

  async checkIfPatientExists(
    userEmail?: string,
    id?: string,
  ): Promise<PatientDocument[]> {
    const res = await this.patientModel
      .find({ emailAddress: userEmail, id: id })
      .exec();
    return res;
  }

  async getPatientsForDoctor(doctorId: string): Promise<PatientDocument[]> {
    try {
      const res = await this.patientModel
        .find({ forDoctorId: doctorId })
        .exec();
      return res;
    } catch (e: any) {
      Logger.error(
        'There was an error getting patients for doctor: ' + e,
        'getPatientsForDoctor()',
        'patient.service.ts',
      );
      return [];
    }
  }
}
