import {
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards-auth.guard';
import { CreateOrUpdatePatientDto, GetPatientDto } from './patient.model';
import { PatientService } from './patient.service';

@Controller('patient')
export class PatientController {
  constructor(private readonly patientService: PatientService) {}
  @Get()
  @UseGuards(JwtAuthGuard)
  healthCheck(): string {
    return 'Hello world from Patient Controller!';
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  createPatient(@Body() createPatient: CreateOrUpdatePatientDto): any {
    return this.patientService.createPatient(createPatient);
  }

  @Get(':patientId')
  @UseGuards(JwtAuthGuard)
  getPatient(@Param('patientId') patientId: string): any {
    return this.patientService.getPatient(patientId);
  }

  @Get('doctor/:doctorId')
  @UseGuards(JwtAuthGuard)
  getAllPatientsForDoctor(@Param('doctorId') doctorId: string): any {
    Logger.debug(
      'Getting patient list for doctor id: ' + doctorId,
      'getAllPatientsForDoctor()',
    );
    return this.patientService.getPatientsForDoctor(doctorId);
  }

  @Post('check-existence')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get patient based on email or id.' })
  postEmailOrIdForPatient(@Body() body: GetPatientDto): any {
    return this.patientService.checkIfPatientExists(body.emailAddress, body.id);
  }
}
