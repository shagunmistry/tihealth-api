import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Role } from '../../shared/enums_decorators/role';

@Schema()
export class BaseRegistrationSchema {
  @ApiProperty({ required: true })
  @Prop()
  userType: 'patient' | 'healthcareWorker';

  @ApiProperty({ required: true })
  @Prop({ required: true })
  firstName: string;

  @ApiProperty({ required: true })
  @Prop({ required: true })
  lastName: string;

  @ApiProperty({ required: true })
  @Prop({ required: true })
  userEmail: string;

  @ApiProperty({ required: true })
  @Prop({ required: true })
  password: string;

  @ApiProperty({ required: true })
  @Prop({ required: true })
  userBirthday: Date;

  @Prop({ required: true })
  roles: Role[];
}

export type RegistrationDocument = BaseRegistrationSchema & Document;
export const RegistrationSchemaForClass = SchemaFactory.createForClass(
  BaseRegistrationSchema,
);

@Schema()
export class PatientRegistrationSchema {
  @ApiProperty({ required: true })
  @Prop()
  addressType: string;

  @ApiProperty({ required: true })
  @Prop()
  city: string;

  @ApiProperty({ required: true })
  @Prop()
  country: string;

  @ApiProperty({ required: true })
  @Prop()
  gender: string;

  @ApiProperty({ required: true })
  @Prop()
  height: string;

  @ApiProperty({ required: true })
  @Prop()
  heightType: string;

  @ApiProperty({ required: true })
  @Prop()
  postcalCode: string;

  @ApiProperty()
  @Prop()
  relationshipAddressType: string;

  @ApiProperty()
  @Prop()
  relationshipCity: string;

  @ApiProperty({ required: true })
  @Prop()
  relationshipCountry: string;

  @ApiProperty({ required: true })
  @Prop()
  relationshipName: string;

  @ApiProperty()
  @Prop()
  relationshipPostcalCode: string;

  @ApiProperty()
  @Prop()
  relationshipState: string;

  @ApiProperty()
  @Prop()
  relationshipStreetName: string;

  @ApiProperty()
  @Prop()
  relationshipStreetNameTwo: string;

  @ApiProperty({ required: true })
  @Prop()
  relationshipType: string;

  @ApiProperty({ required: true })
  @Prop()
  sexuality: string;

  @ApiProperty({ required: true })
  @Prop()
  state: string;

  @ApiProperty({ required: true })
  @Prop()
  streetName: string;

  @ApiProperty()
  @Prop()
  streetNameTwo: string;

  @ApiProperty()
  @Prop()
  userCell: string;

  @ApiProperty()
  @Prop()
  userDescription: string;

  @ApiProperty()
  @Prop()
  userHomeNumbestring: string;

  @ApiProperty()
  @Prop()
  userOfficeNumbestring: string;

  @ApiProperty({ required: true })
  @Prop()
  userWeight: string;

  @ApiProperty({ required: true })
  @Prop()
  userWeightType: string;

  @ApiProperty({ required: true })
  @Prop()
  userId: string;

  @ApiProperty({ required: true })
  @Prop()
  userType: string;
}

export type PatientRegistrationDocument = PatientRegistrationSchema & Document;
export const PatientRegistrationSchemaForClass = SchemaFactory.createForClass(
  PatientRegistrationSchema,
);
