import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  RegistrationSchemaForClass,
  BaseRegistrationSchema,
  PatientRegistrationSchema,
  PatientRegistrationSchemaForClass,
} from './models/registration.schema';
import { RegistrationController } from './registration.controller';
import { RegistrationService } from './registration.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: BaseRegistrationSchema.name, schema: RegistrationSchemaForClass },
      {
        name: PatientRegistrationSchema.name,
        schema: PatientRegistrationSchemaForClass,
      },
    ]),
  ],
  controllers: [RegistrationController],
  providers: [RegistrationService],
})
export class RegistrationModule {}
