import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcryptjs';

import {
  RegistrationDocument,
  BaseRegistrationSchema,
  PatientRegistrationDocument,
  PatientRegistrationSchema,
} from './models/registration.schema';
import { IBasicForm } from '../shared/interfaces.shared';
import { Role } from '../shared/enums_decorators/role';

@Injectable()
export class RegistrationService {
  constructor(
    @InjectModel(BaseRegistrationSchema.name)
    private readonly registrationModel: Model<RegistrationDocument>,
    @InjectModel(PatientRegistrationSchema.name)
    private readonly patientRegistrationModel: Model<PatientRegistrationDocument>,
  ) {}

  /**
   * Get Patient Registration form for step 2.
   * @returns IBasicForm
   */
  getPatientRegistrationForm(): IBasicForm {
    return {
      formId: 'userRegistrationFormPartTwo',
      title: 'Patient Registration',
      fields: [
        {
          fieldID: 'email',
          fieldTitle: 'Email Address',
          fieldType: 'input',
        },
        {
          fieldTitle: 'First Name',
          fieldType: 'input',
          fieldID: 'firstName',
        },
        {
          fieldTitle: 'Last Name',
          fieldType: 'input',
          fieldID: 'lastName',
        },
        {
          fieldID: 'address',
          fieldTitle: 'Address Information',
          fieldType: 'group',
          fields: [
            {
              fieldID: 'streetName',
              fieldTitle: 'Street Name',
              fieldType: 'input',
            },
            {
              fieldID: 'streetNameTwo',
              fieldTitle: 'Street Name Two',
              fieldType: 'input',
              required: false,
            },
            {
              fieldID: 'city',
              fieldTitle: 'City',
              fieldType: 'input',
            },
            {
              fieldID: 'country',
              fieldTitle: 'Country',
              fieldType: 'input',
            },
            {
              fieldID: 'postcalCode',
              fieldTitle: 'Postal Code',
              fieldType: 'input',
            },
            {
              fieldID: 'state',
              fieldTitle: 'State',
              fieldType: 'input',
            },
            {
              fieldID: 'addressType',
              fieldTitle: 'Address Type',
              fieldType: 'dropdown',
              fields: [
                {
                  fieldID: 'userAddressNicknameHome',
                  fieldTitle: 'Home',
                  fieldType: 'label',
                },
                {
                  fieldID: 'userAddressNicknameOffice',
                  fieldTitle: 'Office',
                  fieldType: 'label',
                },
              ],
            },
          ],
        },
        {
          fieldID: 'description',
          fieldTitle: 'Tell us a little bit about the patient?',
          fieldType: 'input',
        },

        {
          fieldID: 'gender',
          fieldTitle: 'Gender?',
          fieldType: 'dropdown',
          fields: [
            {
              fieldID: 'genderTypeMale',
              fieldTitle: 'Male',
              fieldType: 'label',
            },
            {
              fieldID: 'genderTypeFemale',
              fieldTitle: 'Female',
              fieldType: 'label',
            },
            {
              fieldID: 'genderTypeOther',
              fieldTitle: 'Other',
              fieldType: 'label',
            },
          ],
        },
        {
          fieldID: 'sexuality',
          fieldTitle: 'Sexuality?',
          fieldType: 'dropdown',
          fields: [
            {
              fieldID: 'sexualityTypeStraight',
              fieldTitle: 'Straight',
              fieldType: 'label',
            },
            {
              fieldID: 'sexualityTypeLesbian',
              fieldTitle: 'Lesbian',
              fieldType: 'label',
            },
            {
              fieldID: 'sexualityTypeOther',
              fieldTitle: 'Other',
              fieldType: 'label',
            },
          ],
        },
        {
          fieldID: 'userWeightGroup',
          fieldTitle: 'Weight',
          fieldType: 'group',
          fields: [
            {
              fieldID: 'weight',
              fieldTitle: 'Weight?',
              fieldType: 'number',
              required: false,
            },
            {
              fieldID: 'weightType',
              fieldTitle: 'Weight Type',
              fieldType: 'dropdown',
              required: false,
              fields: [
                {
                  fieldID: 'weightTypeLb',
                  fieldTitle: 'Lb',
                  fieldType: 'label',
                },
                {
                  fieldID: 'weightTypeKg',
                  fieldTitle: 'Kg',
                  fieldType: 'label',
                },
              ],
            },
          ],
        },
        {
          fieldID: 'userHeight',
          fieldTitle: 'Height',
          fieldType: 'group',
          fields: [
            {
              fieldID: 'height',
              fieldTitle: 'Height?',
              fieldType: 'number',
              required: false,
            },
            {
              fieldID: 'heightType',
              fieldTitle: 'Height is in: ',
              fieldType: 'dropdown',
              required: false,
              fields: [
                {
                  fieldID: 'heightTypeCm',
                  fieldTitle: 'Centimeters',
                  fieldType: 'label',
                },
                {
                  fieldID: 'heightTypeIn',
                  fieldTitle: 'Inches',
                  fieldType: 'label',
                },
                {
                  fieldID: 'heightTypeFt',
                  fieldTitle: 'Feet',
                  fieldType: 'label',
                },
              ],
            },
          ],
        },
        {
          fieldID: 'userPhoneNumbers',
          fieldTitle: 'Contact Information',
          fieldType: 'group',
          fields: [
            {
              fieldID: 'userCell',
              fieldTitle: 'Cell Phone',
              fieldType: 'number',
              required: false,
            },
            {
              fieldID: 'userHomeNumber',
              fieldTitle: 'Home Phone',
              fieldType: 'number',
              required: false,
            },
            {
              fieldID: 'userOfficeNumber',
              fieldTitle: 'Office Phone',
              fieldType: 'number',
              required: false,
            },
          ],
        },
      ],
    };
  }

  /**
   * Get Step 1 Registratin form
   * @returns IBasicForm
   */
  getStepOneRegistrationForm(): IBasicForm {
    return {
      title: "Let's get started!",
      formId: 'userRegistrationForm',
      fields: [
        {
          fieldType: 'group',
          fieldTitle: 'Who are you?',
          fieldID: 'userType',
          fields: [
            {
              fieldTitle: 'Patient',
              fieldType: 'radioButton',
              fieldID: 'patient',
            },
            {
              fieldTitle: 'Healthcare Worker',
              fieldType: 'radioButton',
              fieldID: 'healthcareWorker',
            },
          ],
        },
        {
          fieldTitle: 'First Name',
          fieldType: 'input',
          fieldID: 'firstName',
        },
        {
          fieldTitle: 'Last Name',
          fieldType: 'input',
          fieldID: 'lastName',
        },
        {
          fieldTitle: 'Email Address',
          fieldType: 'input',
          fieldID: 'userEmail',
        },
        {
          fieldTitle: 'Password',
          fieldType: 'password',
          fieldID: 'password',
        },
        {
          fieldTitle: 'Please select your birthday',
          fieldType: 'date',
          fieldID: 'userBirthday',
        },
      ],
    };
  }

  getICEContactInformationForm(): IBasicForm {
    return {
      title: 'Emergency Contact Information',
      formId: 'emergencyContactInfoForm',
      fields: [
        {
          fieldID: 'userICEContactGroup',
          fieldTitle: 'Emergency Contact Information',
          fieldType: 'group',
          fields: [
            {
              fieldID: 'relationshipType',
              fieldTitle: 'Relationship to the contact',
              fieldType: 'input',
            },
            {
              fieldID: 'relationshipName',
              fieldTitle: 'Name',
              fieldType: 'input',
            },
            {
              fieldID: 'relationshipAddress',
              fieldTitle: 'Contact Address Information',
              fieldType: 'group',
              fields: [
                {
                  fieldID: 'relationshipStreetName',
                  fieldTitle: 'Street Name',
                  fieldType: 'input',
                },
                {
                  fieldID: 'relationshipStreetNameTwo',
                  fieldTitle: 'Street Name Two',
                  fieldType: 'input',
                  required: false,
                },
                {
                  fieldID: 'relationshipCity',
                  fieldTitle: 'City',
                  fieldType: 'input',
                },
                {
                  fieldID: 'relationshipCountry',
                  fieldTitle: 'Country',
                  fieldType: 'input',
                },
                {
                  fieldID: 'relationshipPostcalCode',
                  fieldTitle: 'Postal Code',
                  fieldType: 'input',
                },
                {
                  fieldID: 'relationshipState',
                  fieldTitle: 'State',
                  fieldType: 'input',
                },

                {
                  fieldID: 'relationshipAddressType',
                  fieldTitle: 'Address Type',
                  fieldType: 'dropdown',
                  fields: [
                    {
                      fieldID: 'relationshipAddressNicknameHome',
                      fieldTitle: 'Home',
                      fieldType: 'label',
                    },
                    {
                      fieldID: 'relationshipAddressNicknameOffice',
                      fieldTitle: 'Office',
                      fieldType: 'label',
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    };
  }

  /**
   * Check user base registration by email.
   * @param email
   * @returns
   */
  async checkUserRegistration(email: string): Promise<RegistrationDocument> {
    if (email) {
      const res = await this.registrationModel.findOne({ userEmail: email });
      Logger.log(
        `Checked user registration for ${email}`,
        'RegistrationService checkUserRegistration()',
      );
      if (res && res._id) {
        return res;
      }
    }
    return null;
  }

  /**
   * If user exists in the precheck db, check if they exist in the specific schema DB. If they do, then return true.
   * @param id
   * @param checkUseTypeDb
   * @returns
   */
  async checkUserRegistrationById(
    id: string,
    checkUseTypeDb?: 'patient' | 'heathcareWorker',
  ): Promise<boolean> {
    if (id) {
      const res = await this.registrationModel.findOne({ _id: id });

      Logger.log(
        `Checked user registration for ${id}`,
        'RegistrationService -> checkUserRegistrationById()',
      );

      if (res && res._id) {
        if (checkUseTypeDb) {
          if (checkUseTypeDb === 'patient') {
            const patientRes = await this.patientRegistrationModel.findOne({
              _id: id,
            });
            if (patientRes && patientRes.userId !== '') {
              // This means the patient has already registered before.
              return true;
            }
          } else if (checkUseTypeDb === 'heathcareWorker') {
            // TODO: Check registration in healthcare worker DB.
          }
        }

        return false;
      }
    }
    return false;
  }

  /**
   * create base registration entry for step 1 of registration
   * @param registrationBody
   * @returns
   */
  async createUserRegistration(
    registrationBody: BaseRegistrationSchema,
  ): Promise<{ error: boolean; id: string; message: string }> {
    Logger.log(
      `Registration startd for ${registrationBody.userEmail}`,
      'RegistrationService createUserRegistration()',
    );

    // Hash the password
    registrationBody.password = await bcrypt.hash(
      registrationBody.password,
      12,
    );

    const newRegistration = new this.registrationModel(registrationBody);
    newRegistration.roles.includes(<Role>registrationBody.userType)
      ? null
      : newRegistration.roles.push(<Role>registrationBody.userType);
    return newRegistration
      .save()
      .then((val: RegistrationDocument) => {
        Logger.log(
          `Registration finished for ${registrationBody.userEmail} -- ${val._id}`,
          'RegistrationService createUserRegistration()',
        );
        return {
          error: false,
          id: val._id,
          userName: val.userEmail,
          message: 'Welcome! You have successfully registered to TiHealth!',
        };
      })
      .catch((err: any) => {
        Logger.error(
          `There was an error creating the ${registrationBody.userType} ${registrationBody.userEmail}: ${err}`,
          'RegistrationService createUserRegistration()',
        );

        return {
          error: true,
          id: null,
          message: `There was an error creating the ${registrationBody.userType} ${registrationBody.userEmail}: ${err}`,
        };
      });
  }

  /**
   * Register a patient (step 2 of registration)
   */
  async registerPatient(body: PatientRegistrationSchema): Promise<any> {
    try {
      const patientRegistration = new this.patientRegistrationModel({
        ...body,
        _id: body.userId,
      });
      return patientRegistration
        .save()
        .then((val: PatientRegistrationDocument) => {
          return {
            error: false,
            id: val._id,
            message: `Welcome to TiHealth! You are now registered and free to use the platform as a ${body.userType}`,
          };
        });
    } catch (e: any) {
      Logger.error(
        `There was an error creating the ${body.userType} ${
          body.userId
        }: ${JSON.stringify(e)}`,
        'RegistrationService createUserRegistration()',
      );

      return {
        error: true,
        id: null,
        message: `There was an error creating the ${body.userType} ${
          body.userId
        }: ${JSON.stringify(e)}`,
      };
    }
  }
}
