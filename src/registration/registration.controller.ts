import {
  Body,
  Controller,
  Get,
  HttpException,
  Logger,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import {
  BaseRegistrationSchema,
  PatientRegistrationSchema,
} from './models/registration.schema';
import { IBasicForm } from '../shared/interfaces.shared';
import { JwtAuthGuard } from '../auth/guards-auth.guard';
import { RegistrationService } from './registration.service';

@Controller('registration')
@ApiTags('registration')
export class RegistrationController {
  constructor(private readonly registrationService: RegistrationService) {}

  @Get()
  @ApiOperation({ summary: 'Get Registration Controller health check' })
  getHealth() {
    return 'Hello from Registration controller';
  }

  @Get('form/:step')
  @ApiOperation({ summary: 'Get Registration Form' })
  form(@Param('step', ParseIntPipe) step: number, @Query() query): IBasicForm {
    let formData: IBasicForm;
    if (step === 1) {
      formData = this.registrationService.getStepOneRegistrationForm();
      return formData;
    } else if (
      step === 2 &&
      query.userType === 'patient' &&
      query.userId !== ''
    ) {
      formData = this.registrationService.getPatientRegistrationForm();
      return formData;
    } else {
      throw new HttpException(`Please send a valid request.`, 400);
    }
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'User is good to ready to proceed to step 2.',
  })
  @ApiResponse({
    status: 400,
    description: 'User already exists. Pleae sign in with your email address.',
  })
  async register(
    @Body() registrationBody: BaseRegistrationSchema,
  ): Promise<any> {
    Logger.log(
      `Step 1 Registration started for ${registrationBody.userType} ${registrationBody.userEmail}`,
      'RegistrationController POST register()',
    );

    try {
      const checkUserExistence = await this.registrationService.checkUserRegistration(
        registrationBody.userEmail,
      );
      if (checkUserExistence && checkUserExistence._id !== undefined) {
        return {
          error: true,
          message:
            'User already exists, please sign in with your email address.',
        };
      } else {
        const newUser = await this.registrationService.createUserRegistration(
          registrationBody,
        );
        return {
          error: newUser.error,
          userId: newUser.id,
          stepTwoForm:
            newUser.error && registrationBody.userType === 'patient'
              ? null
              : this.registrationService.getPatientRegistrationForm(),
        };
      }
    } catch (e: any) {
      return {
        error: true,
        message: `There was an error registering user: ${registrationBody.userEmail}. --> ${e.message}`,
      };
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('patient')
  async registerPatient(
    @Body() registerPatient: PatientRegistrationSchema,
  ): Promise<{ error: boolean; id: string | null; message: string }> {
    // Check if the user id exists in Mongo and confirm the patient type for registration
    const userExistsErrorMsg = `Error for Step 2 Registration for ${registerPatient.userType} ${registerPatient.userId} -- There was an error. Please confirm that you are registering a new patient. Please try again later. `;
    const placeOfError = 'RegistrationController POST registerPatient()';

    try {
      const userExists = await this.registrationService.checkUserRegistrationById(
        registerPatient.userId,
        'patient',
      );
      if (!userExists) {
        Logger.log(
          `Step 2 Registration started for ${registerPatient.userType} ${registerPatient.userId}`,
          'RegistrationController POST registerPatient()',
        );
        return await this.registrationService.registerPatient(registerPatient);
      } else {
        Logger.error(userExistsErrorMsg, placeOfError);
        return {
          error: true,
          id: null,
          message: `This user either already exists or the information provided is not correct, please review.`,
        };
      }
    } catch (e: any) {
      Logger.error(
        `Error for Step 2 Registration for ${registerPatient.userType} ${registerPatient.userId}`,
        'RegistrationController POST registerPatient()',
      );
      return {
        error: true,
        id: null,
        message: `There was an error. Please confirm that you are registering a new patient. Please try again later.`,
      };
    }
  }
}
